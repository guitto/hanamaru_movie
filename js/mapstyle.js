function initialize() {
  var mapProp = {
    center:new google.maps.LatLng(35.6787414, 139.7780497),
    zoom:5,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
google.maps.event.addDomListener(window, 'load', initialize);